import { INestApplication } from '@nestjs/common'
import { Test } from '@nestjs/testing'
import exp from 'constants'
import request from 'supertest'
import { getConnection } from 'typeorm'
import { AppModule } from '../src/app.module'
import { AuthorsService } from '../src/authors/authors.service'
import { BooksService } from '../src/books/books.service'
import { BookInput } from '../src/graphql'
import { authorsMock, booksMock } from './lib/data-mock'

describe("Books Resolver (e2e)", () => {
  let app: INestApplication
  let authorsService: AuthorsService
  let booksService: BooksService

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [AppModule],
    })
      .compile();

    app = moduleRef.createNestApplication()
    await app.init()

    authorsService = moduleRef.get(AuthorsService)
    booksService = moduleRef.get(BooksService)
  })

  beforeEach(async () => {
    await getConnection().synchronize(true)
  })

  afterEach(async () => {
    await getConnection().dropDatabase()
  })

  afterAll(async () => {
    await getConnection().close()
  })

  test('deleteAuthorWithBooks should return count of affected rows', async () => {
    const authors = await Promise.all(authorsMock.map(author => authorsService.create(author)))
    const booksData: BookInput[] = [
      { ...booksMock[0], authorIds: [authors[0].id.toString()] },
      { ...booksMock[1], authorIds: [authors[1].id.toString()] },
      { ...booksMock[2], authorIds: [authors[2].id.toString()] },
    ]
    const books = await Promise.all(booksData.map(book => booksService.create(book)))
    await booksService.addAuthor(books[1].id, authors[0].id)

    const { body: { data, errors } } = await request(app.getHttpServer())
      .post('/graphql')
      .send({
        variables: { id: authors[0].id},
        query: `
          mutation DeleteAuthorWithBooks($id: ID!) {
            deleteAuthorWithBooks(id: $id)
          }
        `
      })

    expect(errors).toBeUndefined()
    expect(data).toBeDefined()
    expect(data).toEqual({ deleteAuthorWithBooks: 3 })
  })
})