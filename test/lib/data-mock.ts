import { AuthorInput, BookInput } from '../../src/graphql'

export const authorsMock: AuthorInput[] = [
  {
    firstName: "Marcel",
    lastName: "Proust"
  },
  {
    firstName: "James",
    lastName: "Joyce"
  },
  {
    firstName: "Miguel",
    lastName: "de Cervantes"
  }
]

export const booksMock: Omit<BookInput, 'authorIds'>[] = [
  {
    title: "In Search of Lost Time",
  },
  {
    title: "Ulysses",
  },
  {
    title: "Don Quixote"
  }
]