import { GraphQLResolveInfo } from 'graphql'
export type Maybe<T> = T | null
export type RequireFields<T, K extends keyof T> = { [X in Exclude<keyof T, K>]?: T[X] } &
  { [P in K]-?: NonNullable<T[P]> }
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string
  String: string
  Boolean: boolean
  Int: number
  Float: number
}

export type Query = {
  __typename?: 'Query'
  getAuthor?: Maybe<Author>
  getAuthors: Array<Author>
  getBook?: Maybe<Book>
  getBooks: Array<Book>
  me?: Maybe<Scalars['String']>
}

export type QueryGetAuthorArgs = {
  id: Scalars['ID']
}

export type QueryGetAuthorsArgs = {
  minNumberOfBooks?: Maybe<Scalars['Int']>
  maxNumberOfBooks?: Maybe<Scalars['Int']>
}

export type QueryGetBookArgs = {
  id: Scalars['ID']
}

export type QueryGetBooksArgs = {
  title?: Maybe<Scalars['String']>
}

export type Mutation = {
  __typename?: 'Mutation'
  addAuthor: Book
  createAuthor: Author
  createBook: Book
  deleteAuthor: Scalars['Int']
  deleteAuthorWithBooks: Scalars['Int']
  deleteBook: Scalars['Int']
}

export type MutationAddAuthorArgs = {
  bookId: Scalars['ID']
  authorId: Scalars['ID']
}

export type MutationCreateAuthorArgs = {
  input: AuthorInput
}

export type MutationCreateBookArgs = {
  input: BookInput
}

export type MutationDeleteAuthorArgs = {
  id: Scalars['ID']
}

export type MutationDeleteAuthorWithBooksArgs = {
  id: Scalars['ID']
}

export type MutationDeleteBookArgs = {
  id: Scalars['ID']
}

export type Author = {
  __typename?: 'Author'
  books: Array<Book>
  firstName: Scalars['String']
  id: Scalars['ID']
  lastName: Scalars['String']
}

export type AuthorInput = {
  firstName: Scalars['String']
  lastName: Scalars['String']
}

export type Book = {
  __typename?: 'Book'
  id: Scalars['ID']
  title: Scalars['String']
  authors: Array<Author>
}

export type BookInput = {
  title: Scalars['String']
  authorIds: Array<Scalars['ID']>
}

export type CreateAuthorInput = {
  firstName: Scalars['String']
  lastName: Scalars['String']
}

export type ResolverTypeWrapper<T> = Promise<T> | T

export type StitchingResolver<TResult, TParent, TContext, TArgs> = {
  fragment: string
  resolve: ResolverFn<TResult, TParent, TContext, TArgs>
}

export type Resolver<TResult, TParent = {}, TContext = {}, TArgs = {}> =
  | ResolverFn<TResult, TParent, TContext, TArgs>
  | StitchingResolver<TResult, TParent, TContext, TArgs>

export type ResolverFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => Promise<TResult> | TResult

export type SubscriptionSubscribeFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => AsyncIterator<TResult> | Promise<AsyncIterator<TResult>>

export type SubscriptionResolveFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => TResult | Promise<TResult>

export interface SubscriptionSubscriberObject<
  TResult,
  TKey extends string,
  TParent,
  TContext,
  TArgs
> {
  subscribe: SubscriptionSubscribeFn<{ [key in TKey]: TResult }, TParent, TContext, TArgs>
  resolve?: SubscriptionResolveFn<TResult, { [key in TKey]: TResult }, TContext, TArgs>
}

export interface SubscriptionResolverObject<TResult, TParent, TContext, TArgs> {
  subscribe: SubscriptionSubscribeFn<any, TParent, TContext, TArgs>
  resolve: SubscriptionResolveFn<TResult, any, TContext, TArgs>
}

export type SubscriptionObject<TResult, TKey extends string, TParent, TContext, TArgs> =
  | SubscriptionSubscriberObject<TResult, TKey, TParent, TContext, TArgs>
  | SubscriptionResolverObject<TResult, TParent, TContext, TArgs>

export type SubscriptionResolver<
  TResult,
  TKey extends string,
  TParent = {},
  TContext = {},
  TArgs = {}
> =
  | ((...args: any[]) => SubscriptionObject<TResult, TKey, TParent, TContext, TArgs>)
  | SubscriptionObject<TResult, TKey, TParent, TContext, TArgs>

export type TypeResolveFn<TTypes, TParent = {}, TContext = {}> = (
  parent: TParent,
  context: TContext,
  info: GraphQLResolveInfo
) => Maybe<TTypes> | Promise<Maybe<TTypes>>

export type isTypeOfResolverFn<T = {}> = (
  obj: T,
  info: GraphQLResolveInfo
) => boolean | Promise<boolean>

export type NextResolverFn<T> = () => Promise<T>

export type DirectiveResolverFn<TResult = {}, TParent = {}, TContext = {}, TArgs = {}> = (
  next: NextResolverFn<TResult>,
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => TResult | Promise<TResult>

/** Mapping between all available schema types and the resolvers types */
export type ResolversTypes = {
  String: ResolverTypeWrapper<Scalars['String']>
  Boolean: ResolverTypeWrapper<Scalars['Boolean']>
  Query: ResolverTypeWrapper<{}>
  ID: ResolverTypeWrapper<Scalars['ID']>
  Int: ResolverTypeWrapper<Scalars['Int']>
  Mutation: ResolverTypeWrapper<{}>
  Author: ResolverTypeWrapper<Author>
  AuthorInput: AuthorInput
  Book: ResolverTypeWrapper<Book>
  BookInput: BookInput
  CreateAuthorInput: CreateAuthorInput
}

/** Mapping between all available schema types and the resolvers parents */
export type ResolversParentTypes = {
  String: Scalars['String']
  Boolean: Scalars['Boolean']
  Query: {}
  ID: Scalars['ID']
  Int: Scalars['Int']
  Mutation: {}
  Author: Author
  AuthorInput: AuthorInput
  Book: Book
  BookInput: BookInput
  CreateAuthorInput: CreateAuthorInput
}

export type QueryResolvers<
  ContextType = any,
  ParentType extends ResolversParentTypes['Query'] = ResolversParentTypes['Query']
> = {
  getAuthor?: Resolver<
    Maybe<ResolversTypes['Author']>,
    ParentType,
    ContextType,
    RequireFields<QueryGetAuthorArgs, 'id'>
  >
  getAuthors?: Resolver<
    Array<ResolversTypes['Author']>,
    ParentType,
    ContextType,
    RequireFields<QueryGetAuthorsArgs, never>
  >
  getBook?: Resolver<
    Maybe<ResolversTypes['Book']>,
    ParentType,
    ContextType,
    RequireFields<QueryGetBookArgs, 'id'>
  >
  getBooks?: Resolver<
    Array<ResolversTypes['Book']>,
    ParentType,
    ContextType,
    RequireFields<QueryGetBooksArgs, never>
  >
  me?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>
}

export type MutationResolvers<
  ContextType = any,
  ParentType extends ResolversParentTypes['Mutation'] = ResolversParentTypes['Mutation']
> = {
  addAuthor?: Resolver<
    ResolversTypes['Book'],
    ParentType,
    ContextType,
    RequireFields<MutationAddAuthorArgs, 'bookId' | 'authorId'>
  >
  createAuthor?: Resolver<
    ResolversTypes['Author'],
    ParentType,
    ContextType,
    RequireFields<MutationCreateAuthorArgs, 'input'>
  >
  createBook?: Resolver<
    ResolversTypes['Book'],
    ParentType,
    ContextType,
    RequireFields<MutationCreateBookArgs, 'input'>
  >
  deleteAuthor?: Resolver<
    ResolversTypes['Int'],
    ParentType,
    ContextType,
    RequireFields<MutationDeleteAuthorArgs, 'id'>
  >
  deleteAuthorWithBooks?: Resolver<
    ResolversTypes['Int'],
    ParentType,
    ContextType,
    RequireFields<MutationDeleteAuthorWithBooksArgs, 'id'>
  >
  deleteBook?: Resolver<
    ResolversTypes['Int'],
    ParentType,
    ContextType,
    RequireFields<MutationDeleteBookArgs, 'id'>
  >
}

export type AuthorResolvers<
  ContextType = any,
  ParentType extends ResolversParentTypes['Author'] = ResolversParentTypes['Author']
> = {
  books?: Resolver<Array<ResolversTypes['Book']>, ParentType, ContextType>
  firstName?: Resolver<ResolversTypes['String'], ParentType, ContextType>
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>
  lastName?: Resolver<ResolversTypes['String'], ParentType, ContextType>
  __isTypeOf?: isTypeOfResolverFn<ParentType>
}

export type BookResolvers<
  ContextType = any,
  ParentType extends ResolversParentTypes['Book'] = ResolversParentTypes['Book']
> = {
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>
  title?: Resolver<ResolversTypes['String'], ParentType, ContextType>
  authors?: Resolver<Array<ResolversTypes['Author']>, ParentType, ContextType>
  __isTypeOf?: isTypeOfResolverFn<ParentType>
}

export type Resolvers<ContextType = any> = {
  Query?: QueryResolvers<ContextType>
  Mutation?: MutationResolvers<ContextType>
  Author?: AuthorResolvers<ContextType>
  Book?: BookResolvers<ContextType>
}

/**
 * @deprecated
 * Use "Resolvers" root object instead. If you wish to get "IResolvers", add "typesPrefix: I" to your config.
 */
export type IResolvers<ContextType = any> = Resolvers<ContextType>
