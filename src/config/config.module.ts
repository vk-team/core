import { Module } from '@nestjs/common'
import { ConfigModule as NestConfigModule, ConfigService as NestConfigService } from '@nestjs/config'
import { NEST_CONFIG_SERVICE } from './config.constants'
import { ConfigService } from './config.service'
import { getEnv } from './lib'

@Module({
  imports: [
    NestConfigModule.forRoot({
      load: [getEnv],
    }),
  ],
  providers: [
    {
      provide: NEST_CONFIG_SERVICE,
      useClass: NestConfigService,
    },
    ConfigService,
  ],
  exports: [ConfigService],
})
export class ConfigModule {}
