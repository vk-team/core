const getConfig = () => ({
  mysqlUrl: process.env.MYSQL_URL,
})

export default getConfig
