import { Inject, Injectable } from '@nestjs/common'
import { ConfigService as NestConfigService } from '@nestjs/config'
import { NEST_CONFIG_SERVICE } from './config.constants'
import { getEnv } from './lib'

@Injectable()
export class ConfigService {
  constructor(@Inject(NEST_CONFIG_SERVICE) readonly nestConfigService: NestConfigService) {}

  get(key: keyof ReturnType<typeof getEnv>) {
    return this.nestConfigService.get(key)
  }
}
