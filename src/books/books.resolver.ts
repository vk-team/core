import { Args, Mutation, Parent, Query, ResolveField, Resolver } from '@nestjs/graphql'
import {
  BookInput,
  MutationAddAuthorArgs,
  MutationDeleteBookArgs,
  QueryGetAuthorArgs,
  QueryGetBooksArgs,
} from '../graphql'
import { BookEntity } from './book.entity'
import { BooksService } from './books.service'

@Resolver(() => BookEntity)
export class BooksResolver {
  constructor(private booksService: BooksService) {}

  @Query()
  async getBooks(@Args() args: QueryGetBooksArgs) {
    return await this.booksService.findMany(args)
  }

  @Query()
  async getBook(@Args() { id }: QueryGetAuthorArgs) {
    return (await this.booksService.findOne({ id: parseInt(id) })) || null
  }

  @Mutation()
  createBook(@Args('input') input: BookInput) {
    return this.booksService.create(input)
  }

  @Mutation()
  deleteBook(@Args() { id }: MutationDeleteBookArgs) {
    return this.booksService.removeByIds(parseInt(id))
  }

  @Mutation()
  addAuthor(@Args() { bookId, authorId }: MutationAddAuthorArgs) {
    return this.booksService.addAuthor(parseInt(bookId), parseInt(authorId))
  }

  @ResolveField()
  async authors(@Parent() { id }: BookEntity) {
    const book = await this.booksService.findOne({ id })

    return book.authors
  }
}
