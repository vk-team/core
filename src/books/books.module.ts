import { forwardRef, Module } from '@nestjs/common'
import { TypeOrmModule } from '@nestjs/typeorm'
import { AuthorsModule } from '../authors'
import { BookEntity } from './book.entity'
import { BooksResolver } from './books.resolver'
import { BooksService } from './books.service'

@Module({
  imports: [TypeOrmModule.forFeature([BookEntity]), forwardRef(() => AuthorsModule)],
  providers: [BooksResolver, BooksService],
  exports: [BooksResolver, BooksService],
})
export class BooksModule {}
