import { Injectable } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { FindConditions, In, Raw, Repository } from 'typeorm'
import { BookInput, QueryGetBooksArgs } from '../graphql'
import { BookEntity } from './book.entity'

@Injectable()
export class BooksService {
  constructor(@InjectRepository(BookEntity) private bookRepository: Repository<BookEntity>) {}

  async create({ authorIds, ...data }: BookInput) {
    const qb = this.bookRepository.createQueryBuilder('book')

    const {
      identifiers: [{ id }],
    } = await qb.insert().values(data).execute()
    await qb.relation('authors').of(id).add(authorIds)

    return this.findOne({ id })
  }

  qb() {
    return this.bookRepository.createQueryBuilder('book')
  }

  async addAuthor(id: number, authorId: number) {
    const qb = this.bookRepository.createQueryBuilder('book')

    await qb.relation('authors').of(id).add(authorId)

    return this.findOne({ id })
  }

  findOne({ id }: { id: number }) {
    return this.bookRepository.findOne({ id })
  }

  findMany({ title }: QueryGetBooksArgs = {}) {
    const where: FindConditions<BookEntity> = {}

    if (title) {
      where.title = Raw((alias) => `${alias} ILIKE '${title}%'`)
    }

    return this.bookRepository.find({ where })
  }

  async removeByIds(id: number | number[]) {
    const where: FindConditions<BookEntity> = {}

    if (Array.isArray(id)) {
      where.id = In(id)
    } else {
      where.id = id
    }

    const { affected } = await this.bookRepository.delete(where)
    return affected
  }
}
