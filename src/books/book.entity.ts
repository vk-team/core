import { Field, ID, ObjectType } from '@nestjs/graphql'
import { Column, Entity, JoinTable, ManyToMany, PrimaryGeneratedColumn } from 'typeorm'
import { AuthorEntity } from '../authors'

@ObjectType('Book')
@Entity()
export class BookEntity {
  @Field(() => ID)
  @PrimaryGeneratedColumn()
  id: number

  @Field(() => String)
  @Column()
  title: string

  @ManyToMany(() => AuthorEntity, (author) => author.books)
  @JoinTable()
  authors: Promise<AuthorEntity[]>
}
