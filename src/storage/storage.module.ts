import { Module } from '@nestjs/common'
import { TypeOrmModule } from '@nestjs/typeorm'
import * as path from 'path'
import { MysqlConnectionOptions } from 'typeorm/driver/mysql/MysqlConnectionOptions'
import { ConfigModule } from '../config/config.module'
import { ConfigService } from '../config/config.service'

@Module({
  imports: [
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (configService: ConfigService): MysqlConnectionOptions => ({
        type: 'mysql',
        port: 6000,
        url: configService.get('mysqlUrl'),
        database: configService.get('mysqlUrl').match(/(?!.*\/)(.*)/)[1],
        entities: [path.resolve(`${__dirname}/../**/*.entity{.ts,.js}`)],
        synchronize: true,
      }),
    }),
  ],
})
export class StorageModule {}
