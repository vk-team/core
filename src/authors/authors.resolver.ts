import { Args, Mutation, Parent, Query, ResolveField, Resolver } from '@nestjs/graphql'
import {
  AuthorInput,
  MutationDeleteAuthorArgs,
  MutationDeleteAuthorWithBooksArgs,
  QueryGetAuthorArgs,
  QueryGetAuthorsArgs,
} from '../graphql'
import { AuthorEntity } from './author.entity'
import { AuthorsService } from './authors.service'

@Resolver(() => AuthorEntity)
export class AuthorsResolver {
  constructor(private authorsService: AuthorsService) {}

  @Query()
  async getAuthor(@Args() { id }: QueryGetAuthorArgs) {
    return (await this.authorsService.findOne({ id: parseInt(id) })) || null
  }

  @Query()
  async getAuthors(@Args() args: QueryGetAuthorsArgs) {
    return this.authorsService.findMany(args)
  }

  @Mutation()
  createAuthor(@Args('input') input: AuthorInput) {
    return this.authorsService.create(input)
  }

  @Mutation()
  deleteAuthor(@Args() { id }: MutationDeleteAuthorArgs) {
    return this.authorsService.removeById(parseInt(id))
  }

  @Mutation()
  async deleteAuthorWithBooks(@Args() args: MutationDeleteAuthorWithBooksArgs) {
    const id = parseInt(args.id)
    return this.authorsService.removeAuthorWithBooks(id)
  }

  @ResolveField()
  async books(@Parent() { id }: AuthorEntity) {
    const author = await this.authorsService.findOne({ id })

    return author.books
  }
}
