import { Field, ID, ObjectType } from '@nestjs/graphql'
import { Column, Entity, ManyToMany, PrimaryGeneratedColumn } from 'typeorm'
import { BookEntity } from '../books/book.entity'

@ObjectType('Author')
@Entity()
export class AuthorEntity {
  @Field(() => ID)
  @PrimaryGeneratedColumn()
  id: number

  @Field(() => String)
  @Column({ unique: true })
  firstName: string

  @Field(() => String)
  @Column({ unique: true })
  lastName: string

  @ManyToMany(() => BookEntity, (books) => books.authors)
  books: Promise<BookEntity[]>
}
