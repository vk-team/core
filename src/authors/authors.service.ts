import { Injectable } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { In, Repository } from 'typeorm'
import { FindConditions } from 'typeorm/find-options/FindConditions'
import { BooksService } from '../books/books.service'
import { AuthorInput, QueryGetAuthorsArgs } from '../graphql'
import { AuthorEntity } from './author.entity'

@Injectable()
export class AuthorsService {
  constructor(
    @InjectRepository(AuthorEntity) private authorRepository: Repository<AuthorEntity>,
    private booksService: BooksService
  ) {}

  async create(data: AuthorInput) {
    const { id } = await this.authorRepository.save(this.authorRepository.create(data))
    return this.findOne({ id })
  }

  findOne({ id }: { id: number }) {
    return this.authorRepository.findOne({ id })
  }

  async findMany({
    ids,
    maxNumberOfBooks,
    minNumberOfBooks,
  }: { ids?: number[] } & QueryGetAuthorsArgs) {
    const qb = this.authorRepository.createQueryBuilder('author')
    const where: FindConditions<AuthorEntity> = {}

    if (ids) {
      where.id = In(ids)
    }

    if (minNumberOfBooks || maxNumberOfBooks) {
      qb.innerJoin('author.books', 'book').groupBy('author.id')
    }

    if (minNumberOfBooks) {
      qb.andHaving(':minBooks <= count(*)', { minBooks: minNumberOfBooks })
    }

    if (maxNumberOfBooks) {
      qb.andHaving('count(*) <= :maxBooks', { maxBooks: maxNumberOfBooks })
    }

    return qb.getMany()
  }

  async removeAuthorWithBooks(id: number) {
    const author = await this.authorRepository.findOne({ id })
    // use this count because join table delete automatically on delete author
    const books = await author.books

    const deleteRawsCount = await this.removeById(id)

    const onlyAuthorBooksIds = (await this.booksService.qb()
      .select()
      .leftJoinAndSelect('book.authors', 'author')
      .groupBy('book.id')
      .addGroupBy('author.id')
      .execute()).filter(raw => raw.author_id === null).map(raw => raw.book_id)

    await this.booksService.removeByIds(onlyAuthorBooksIds)

    return deleteRawsCount + books.length
  }

  async removeById(id: number) {
    const { affected } = await this.authorRepository.delete({ id })
    return affected
  }
}
