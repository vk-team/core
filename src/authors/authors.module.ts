import { forwardRef, Module } from '@nestjs/common'
import { TypeOrmModule } from '@nestjs/typeorm'
import { BooksModule } from '../books/books.module'
import { AuthorEntity } from './author.entity'
import { AuthorsResolver } from './authors.resolver'
import { AuthorsService } from './authors.service'

@Module({
  imports: [TypeOrmModule.forFeature([AuthorEntity]), forwardRef(() => BooksModule)],
  providers: [AuthorsResolver, AuthorsService],
  exports: [AuthorsResolver, AuthorsService],
})
export class AuthorsModule {}
