import { Module } from '@nestjs/common'
import { GraphQLModule } from '@nestjs/graphql'
import { AuthorsModule } from './authors'
import { BooksModule } from './books/books.module'
import { StorageModule } from './storage'

@Module({
  imports: [
    GraphQLModule.forRoot({
      playground: true,
      path: '/',
      typePaths: ['./**/*.graphql'],
    }),
    AuthorsModule,
    StorageModule,
    BooksModule,
  ],
})
export class AppModule {}
